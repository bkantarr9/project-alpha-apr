# Feature 15-

from django.forms import ModelForm
from tasks.models import Task


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "is_completed",
            "start_date",
            "due_date",
            "project",
            "assignee",
            "id",
        ]
        # exclude = ("is_completed",)
