# Generated by Django 5.0.1 on 2024-02-06 01:14

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("projects", "0002_rename_project_projects"),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.RenameModel(
            old_name="Projects",
            new_name="Project",
        ),
    ]
